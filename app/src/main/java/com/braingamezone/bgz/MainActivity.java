package com.braingamezone.bgz;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ssomai.android.scalablelayout.ScalableLayout;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends Activity implements View.OnClickListener {
    WebView mWebView;
    TextView hintOpenBtn;
    TextView memoOpenBtn;

    RelativeLayout hintLayout;
    ImageView hintCloseTxt;
    TextView hintCodeTxt;
    ScalableLayout hintPanelLayout;
    TextView[] numBtn = new TextView[10];
    TextView clearBtn;
    TextView BsBtn;
    ScalableLayout hintContentLayout;
    TextView hintContentTxt;
    TextView hintContentCloseBtn;
    TextView hintAnsBtn;

    RelativeLayout memoLayout;
    ImageView memoCloseTxt;
    TextView memoClearBtn;
    WritingView memoView;

    String curHintCode;
    String curHint;
    String curAns;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("PAGE", "MAIN");
        setContentView(R.layout.activity_main);

        // 웹뷰 셋팅
        mWebView = (WebView) findViewById(R.id.webView);//xml 자바코드 연결
        mWebView.getSettings().setJavaScriptEnabled(true);//자바스크립트 허용

        mWebView.clearHistory();
        mWebView.clearCache(true);
        mWebView.clearView();

        String url = Data.SERVER_ADDRESS + "clientApp.php?store_srl=%s&room_number=%s&theme_srl=%s&packet=B14X0LS0000000000000AC0000000E&force_play=Y";
        mWebView.loadUrl(String.format(url, Data.storeSrl, Data.roomNumber, Data.themeSrl));//웹뷰 실행
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.setWebChromeClient(new WebChromeClient());//웹뷰에 크롬 사용 허용//이 부분이 없으면 크롬에서 alert가 뜨지 않음
        mWebView.setWebViewClient(new WebViewClientClass());//새창열기 없이 웹뷰 내에서 다시 열기//페이지 이동 원활히 하기위해 사용

        hintOpenBtn = (TextView) findViewById(R.id.hint_open_btn);
        memoOpenBtn = (TextView) findViewById(R.id.memo_open_btn);


        hintLayout = (RelativeLayout) findViewById(R.id.hint_layout);
        hintCloseTxt = (ImageView) findViewById(R.id.hint_close_btn);
        hintCodeTxt = (TextView) findViewById(R.id.hint_code_txt);

        hintPanelLayout = (ScalableLayout) findViewById(R.id.hint_panel_layout);

        numBtn[0] = (TextView) findViewById(R.id.num0_btn);
        numBtn[1] = (TextView) findViewById(R.id.num1_btn);
        numBtn[2] = (TextView) findViewById(R.id.num2_btn);
        numBtn[3] = (TextView) findViewById(R.id.num3_btn);
        numBtn[4] = (TextView) findViewById(R.id.num4_btn);
        numBtn[5] = (TextView) findViewById(R.id.num5_btn);
        numBtn[6] = (TextView) findViewById(R.id.num6_btn);
        numBtn[7] = (TextView) findViewById(R.id.num7_btn);
        numBtn[8] = (TextView) findViewById(R.id.num8_btn);
        numBtn[9] = (TextView) findViewById(R.id.num9_btn);

        clearBtn = (TextView) findViewById(R.id.numc_btn);
        BsBtn = (TextView) findViewById(R.id.numbs_btn);

        hintContentLayout = (ScalableLayout) findViewById(R.id.hint_content_layout);
        hintContentTxt = (TextView) findViewById(R.id.hint_content_txt);
        hintContentCloseBtn = (TextView) findViewById(R.id.hint_content_close_btn);
        hintAnsBtn = (TextView) findViewById(R.id.hint_ans_btn);


        memoLayout = (RelativeLayout) findViewById(R.id.memo_layout);
        memoCloseTxt = (ImageView) findViewById(R.id.memo_close_btn);
        memoClearBtn = (TextView) findViewById(R.id.memo_clear_btn);
        memoView = (WritingView) findViewById(R.id.memo_view);

        hintOpenBtn.setOnClickListener(this);
        memoOpenBtn.setOnClickListener(this);

        hintLayout.setOnClickListener(this);
        hintCloseTxt.setOnClickListener(this);
        clearBtn.setOnClickListener(this);
        BsBtn.setOnClickListener(this);
        for (int i = 0; i < 10; i++)
            numBtn[i].setOnClickListener(this);

        hintContentCloseBtn.setOnClickListener(this);
        hintAnsBtn.setOnClickListener(this);

        memoLayout.setOnClickListener(this);
        memoCloseTxt.setOnClickListener(this);
        memoClearBtn.setOnClickListener(this);

        curHintCode = "";
        hintCodeTxt.setText("힌트코드를 입력하세요");

        curHint = "";
        curAns = "";
        hintContentTxt.setText("");

        hintLayout.setVisibility(View.INVISIBLE);
        memoLayout.setVisibility(View.INVISIBLE);
        hintPanelLayout.setVisibility(View.VISIBLE);
        hintContentLayout.setVisibility(View.INVISIBLE);
        hintAnsBtn.setVisibility(View.VISIBLE);
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {//뒤로가기 버튼 이벤트
//        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {//웹뷰에서 뒤로가기 버튼을 누르면 뒤로가짐
////            mWebView.goBack();
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    private class WebViewClientClass extends WebViewClient {//페이지 이동

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.d("check URL", url);
            view.loadUrl(url);
            return true;
        }
    }


    public void onClick(View v) {
        int id = v.getId();

        Dialog a;

        switch (id) {
            case R.id.hint_open_btn:
                hintLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.memo_open_btn:
                memoLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.hint_layout:
            case R.id.memo_layout:
                break;
            case R.id.hint_close_btn:
                hintLayout.setVisibility(View.INVISIBLE);
                break;
            case R.id.memo_close_btn:
                memoLayout.setVisibility(View.INVISIBLE);
                break;
            case R.id.memo_clear_btn:
                a = new AlertDialog.Builder(this)
                        .setTitle("확인")
                        .setMessage("메모를 모두 지우시겠습니까?")
                        .setPositiveButton("네", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                memoView.clear();
                            }
                        })
                        .setNegativeButton("아니오", null).show();
                a.setCanceledOnTouchOutside(false);
                break;
            case R.id.numc_btn:
                curHintCode = "";
                hintCodeTxt.setText("힌트코드를 입력하세요");
                break;
            case R.id.numbs_btn:
                deleteString();
                break;
            case R.id.hint_content_close_btn:
                hintPanelLayout.setVisibility(View.VISIBLE);
                hintContentLayout.setVisibility(View.INVISIBLE);
                hintContentTxt.setText("");
                hintAnsBtn.setVisibility(View.VISIBLE);
                curHintCode = "";
                hintCodeTxt.setText("힌트코드를 입력하세요");
                break;
            case R.id.hint_ans_btn:
                a = new AlertDialog.Builder(this)
                        .setTitle("확인")
                        .setMessage("정답을 확인하시겠습니까? (추가적인 힌트 차감은 없습니다)")
                        .setPositiveButton("네", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                hintContentTxt.setText(curAns);
                                hintAnsBtn.setVisibility(View.INVISIBLE);
                            }
                        })
                        .setNegativeButton("아니오", null).show();
                a.setCanceledOnTouchOutside(false);
            default:
                for (int i = 0; i < 10; i++) {
                    if (id == numBtn[i].getId()) {
                        addString(i);
                        break;
                    }
                }
                break;
        }
    }

    void addString(int num) {
        curHintCode = curHintCode + num;
        String pwResult = "";
        for (int i = 0; i < 5; i++) {
            if (i < curHintCode.length())
                pwResult += "●";
            else
                pwResult += "○";
        }
        hintCodeTxt.setText(pwResult);

        if (curHintCode.length() == 5) {
            getHint c = new getHint();
            c.execute();
        }
    }

    void deleteString() {
        if (curHintCode.length() == 0)
            return;
        curHintCode = curHintCode.substring(0, curHintCode.length() - 1);
        String pwResult = "";
        for (int i = 0; i < 5; i++) {
            if (i < curHintCode.length())
                pwResult += "●";
            else
                pwResult += "○";
        }
        if (curHintCode.length() == 0)
            hintCodeTxt.setText("힌트코드를 입력하세요");
        else
            hintCodeTxt.setText(pwResult);
    }


    private class getHint extends AsyncTask<Void, Void, String> {
        String TAG = "GetClassSendPost !!";

        int HintResult;

        getHint() {
        }

        protected String doInBackground(Void... unused) {
            String content = executeClient();
            return content;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            if (HintResult == 1) {
                hintPanelLayout.setVisibility(View.INVISIBLE);
                hintContentLayout.setVisibility(View.VISIBLE);
                hintContentTxt.setText(curHint);
                hintAnsBtn.setVisibility(View.VISIBLE);
            } else {
                hintCodeTxt.setText("잘못된 힌트코드!");
                curHintCode = "";
            }
        }

        private String getHintInfo() {
            Log.d(TAG, "getClass post start");

            ArrayList<NameValuePair> post = new ArrayList<NameValuePair>();
            post.add(new BasicNameValuePair("store_srl", Data.storeSrl + ""));
            post.add(new BasicNameValuePair("room_number", Data.roomNumber + ""));
            post.add(new BasicNameValuePair("theme_srl", Data.themeSrl + ""));
            post.add(new BasicNameValuePair("check", "1"));
            post.add(new BasicNameValuePair("checkCode", curHintCode));
            post.add(new BasicNameValuePair("route", Data.wildWestRoute + ""));

            // Log.e("json data", data.toString());
            // 연결 HttpClient 객체 생성
            HttpClient client;
            client = new DefaultHttpClient();

            // 객체 연결 설정 부분, 연결 최대시간 등등
            HttpParams params = client.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 10000);
            HttpConnectionParams.setSoTimeout(params, 10000);

            // Post객체 생성
            HttpPost httpPost = new HttpPost(Data.SERVER_ADDRESS + "/get_hint_app.php");
            try {

                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(post, "UTF-8");
                httpPost.setEntity(entity);
                HttpResponse response = client.execute(httpPost);
                HttpEntity http_entity = response.getEntity();
                return EntityUtils.toString(http_entity);

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        // 실제 전송하는 부분
        public String executeClient() {
            String respond;
            respond = getHintInfo();

            Log.d(TAG, respond);
            try {
                JSONObject json = new JSONObject(respond);
                HintResult = json.getInt("result");
                curHint = json.getString("hint");
                curAns = json.getString("ans");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}
