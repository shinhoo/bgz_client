package com.braingamezone.bgz;

public class DB_Store {
    int storeSrl;
    String storeName;
    int roomSrl;
    String roomName;
    int roomNumber;
    int cateSrl;
    int themeSrl;

    public DB_Store(int storeSrl, String storeName, int roomSrl, String roomName, int roomNumber, int cateSrl, int themeSrl) {
        this.storeSrl = storeSrl;
        this.storeName = storeName;
        this.roomSrl = roomSrl;
        this.roomName = roomName;
        this.roomNumber = roomNumber;
        this.cateSrl = cateSrl;
        this.themeSrl = themeSrl;
    }

    public int getStoreSrl() {
        return storeSrl;
    }

    public void setStoreSrl(int storeSrl) {
        this.storeSrl = storeSrl;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public int getRoomSrl() {
        return roomSrl;
    }

    public void setRoomSrl(int roomSrl) {
        this.roomSrl = roomSrl;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getCateSrl() {
        return cateSrl;
    }

    public void setCateSrl(int cateSrl) {
        this.cateSrl = cateSrl;
    }

    public int getThemeSrl() {
        return themeSrl;
    }

    public void setThemeSrl(int themeSrl) {
        this.themeSrl = themeSrl;
    }
}
