package com.braingamezone.bgz;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


public class InitActivity extends Activity implements
        View.OnClickListener, AdapterView.OnItemSelectedListener {

    ArrayList<DB_Store> storeRooms;
    Spinner roomSpinner;
    TextView roomSpinnerTxt;
    TextView battleModeBtn;
    TextView wildWestModeBtn;
    TextView startBtn;
    ArrayAdapter<String> adapter;
    String[] stringLists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("PAGE", "INIT");
        setContentView(R.layout.activity_init);

        roomSpinner = (Spinner) findViewById(R.id.room_spinner);
        roomSpinnerTxt = (TextView) findViewById(R.id.room_spinner_txt);
        battleModeBtn = (TextView) findViewById(R.id.battle_mode_btn);
        wildWestModeBtn = (TextView) findViewById(R.id.wildwest_mode_btn);
        startBtn = (TextView) findViewById(R.id.start_btn);

        battleModeBtn.setOnClickListener(this);
        wildWestModeBtn.setOnClickListener(this);
        startBtn.setOnClickListener(this);

        startBtn.setVisibility(View.INVISIBLE);

        battleModeBtn.setVisibility(View.INVISIBLE);
        battleModeBtn.setTextColor(0xff5c5c5c);
        battleModeBtn.setText("배틀모드 Off");
        Data.isBattle = 0;

        wildWestModeBtn.setVisibility(View.INVISIBLE);
        wildWestModeBtn.setText("갱");
        Data.wildWestRoute = 0;

        GetStoreInfo c = new GetStoreInfo();
        c.execute();
    }

    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
        switch (parent.getId()) {
            case R.id.room_spinner:
                DB_Store cur = storeRooms.get(position);
                Data.storeSrl = cur.storeSrl;
                Data.storeName = cur.storeName;
                Data.roomSrl = cur.roomSrl;
                Data.roomName = cur.roomName;
                Data.roomNumber = cur.roomNumber;
                Data.cateSrl = cur.cateSrl;
                Data.themeSrl = cur.themeSrl;
                startBtn.setVisibility(View.VISIBLE);
                roomSpinnerTxt.setText(cur.storeName + " - " + cur.roomName);

                if (cur.storeSrl == 1 && (cur.roomNumber == 2 || cur.roomNumber == 3))
                    battleModeBtn.setVisibility(View.VISIBLE);
                else
                    battleModeBtn.setVisibility(View.INVISIBLE);

                if (cur.storeSrl == 2 && cur.roomNumber == 6)
                    wildWestModeBtn.setVisibility(View.VISIBLE);
                else
                    wildWestModeBtn.setVisibility(View.INVISIBLE);

                battleModeBtn.setTextColor(0xff5c5c5c);
                battleModeBtn.setText("배틀모드 Off");
                Data.isBattle = 0;

                wildWestModeBtn.setText("갱");
                Data.wildWestRoute = 0;
                break;
        }
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.start_btn:
                StartSession c = new StartSession();
                c.execute();
                break;
            case R.id.battle_mode_btn:
                if (Data.isBattle == 0) {
                    battleModeBtn.setTextColor(0xffffffff);
                    battleModeBtn.setText("배틀모드 On");
                    Data.isBattle = 1;
                } else {
                    battleModeBtn.setTextColor(0xff5c5c5c);
                    battleModeBtn.setText("배틀모드 Off");
                    Data.isBattle = 0;
                }
                break;
            case R.id.wildwest_mode_btn:
                if (Data.wildWestRoute == 0) {
                    wildWestModeBtn.setText("보안관");
                    Data.wildWestRoute = 1;
                } else {
                    wildWestModeBtn.setText("갱");
                    Data.wildWestRoute = 0;
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    void setRoomSpinner() {
        stringLists = new String[storeRooms.size()];
        int i = 0;
        for (DB_Store store : storeRooms) {
            stringLists[i] = "  " + store.storeName + " - " + store.roomName;
            i++;
        }

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item,
                stringLists);

        roomSpinner.setAdapter(adapter);
        roomSpinner.setOnItemSelectedListener(this);

        roomSpinner.setSelection(0);
    }


    private class GetStoreInfo extends AsyncTask<Void, Void, String> {
        String TAG = "GetClassSendPost !!";

        GetStoreInfo() {
        }

        protected String doInBackground(Void... unused) {
            String content = executeClient();
            return content;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            setRoomSpinner();
        }

        private String getStoreInfo() {
            Log.d(TAG, "getClass post start");

            ArrayList<NameValuePair> post = new ArrayList<NameValuePair>();
            // Log.e("json data", data.toString());
            // 연결 HttpClient 객체 생성
            HttpClient client;
            client = new DefaultHttpClient();

            // 객체 연결 설정 부분, 연결 최대시간 등등
            HttpParams params = client.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 10000);
            HttpConnectionParams.setSoTimeout(params, 10000);

            // Post객체 생성
            HttpPost httpPost = new HttpPost(Data.SERVER_ADDRESS + "/getStoreInfo.php");
            try {

                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(post, "UTF-8");
                httpPost.setEntity(entity);
                HttpResponse response = client.execute(httpPost);
                HttpEntity http_entity = response.getEntity();
                return EntityUtils.toString(http_entity);

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        // 실제 전송하는 부분
        public String executeClient() {
            String respond;
            respond = getStoreInfo();
            storeRooms = new ArrayList<DB_Store>();

            try {
                JSONArray json = new JSONArray(respond);
                JSONObject object;
                for (int i = 0; i < json.length(); i++) {
                    object = json.getJSONObject(i);

                    int storeSrl = object.getInt("store_srl");
                    String storeName = object.getString("store_name");
                    int roomSrl = object.getInt("room_srl");
                    String roomName = object.getString("room_name");
                    int roomNumber = object.getInt("room_number");
                    int cateSrl = object.getInt("cate_srl");
                    int themeSrl = object.getInt("theme_srl");

                    Log.d(TAG, storeName + " " + roomName);

                    storeRooms.add(new DB_Store(storeSrl, storeName, roomSrl, roomName, roomNumber, cateSrl, themeSrl));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
    }


    private class StartSession extends AsyncTask<Void, Void, String> {
        String TAG = "GetClassSendPost !!";

        StartSession() {
        }

        protected String doInBackground(Void... unused) {
            String content = executeClient();
            return content;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Intent intent = new Intent(InitActivity.this, MainActivity.class);
            startActivity(intent);
            overridePendingTransition(0, 0);
        }

        private String getStartSession() {
            Log.d(TAG, "getClass post start");

            ArrayList<NameValuePair> post = new ArrayList<NameValuePair>();
            post.add(new BasicNameValuePair("store_srl", Data.storeSrl + ""));
            post.add(new BasicNameValuePair("room_number", Data.roomNumber + ""));
            post.add(new BasicNameValuePair("isBattle", Data.isBattle + ""));
            post.add(new BasicNameValuePair("route", Data.wildWestRoute + ""));

            // Log.e("json data", data.toString());
            // 연결 HttpClient 객체 생성
            HttpClient client;
            client = new DefaultHttpClient();

            // 객체 연결 설정 부분, 연결 최대시간 등등
            HttpParams params = client.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 10000);
            HttpConnectionParams.setSoTimeout(params, 10000);

            // Post객체 생성
            HttpPost httpPost = new HttpPost(Data.SERVER_ADDRESS + "/init_session_app.php");
            try {

                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(post, "UTF-8");
                httpPost.setEntity(entity);
                HttpResponse response = client.execute(httpPost);
                HttpEntity http_entity = response.getEntity();
                return EntityUtils.toString(http_entity);

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        // 실제 전송하는 부분
        public String executeClient() {
            String respond;
            respond = getStartSession();

            Log.d(TAG, respond);

            return null;
        }
    }
}
