package com.braingamezone.bgz;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class PWActivity extends Activity implements View.OnClickListener {
    TextView passwordTxt;
    TextView[] numBtn = new TextView[10];
    TextView clearBtn;
    TextView BsBtn;

    String curPW;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("PAGE", "PW");
        setContentView(R.layout.activity_pw);

        passwordTxt = (TextView) findViewById(R.id.password_txt);

        numBtn[0] = (TextView) findViewById(R.id.num0_btn);
        numBtn[1] = (TextView) findViewById(R.id.num1_btn);
        numBtn[2] = (TextView) findViewById(R.id.num2_btn);
        numBtn[3] = (TextView) findViewById(R.id.num3_btn);
        numBtn[4] = (TextView) findViewById(R.id.num4_btn);
        numBtn[5] = (TextView) findViewById(R.id.num5_btn);
        numBtn[6] = (TextView) findViewById(R.id.num6_btn);
        numBtn[7] = (TextView) findViewById(R.id.num7_btn);
        numBtn[8] = (TextView) findViewById(R.id.num8_btn);
        numBtn[9] = (TextView) findViewById(R.id.num9_btn);

        clearBtn = (TextView) findViewById(R.id.numc_btn);
        BsBtn = (TextView) findViewById(R.id.numbs_btn);


        clearBtn.setOnClickListener(this);
        BsBtn.setOnClickListener(this);
        for (int i = 0; i < 10; i++)
            numBtn[i].setOnClickListener(this);

        curPW = "";
        passwordTxt.setText("비밀번호를 입력하세요");
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.numc_btn:
                curPW = "";
                passwordTxt.setText("비밀번호를 입력하세요");
                break;
            case R.id.numbs_btn:
                deleteString();
                break;
            default:
                for (int i = 0; i < 10; i++) {
                    if (id == numBtn[i].getId()) {
                        addString(i);
                        break;
                    }
                }
                break;
        }
    }

    void addString(int num) {
        curPW = curPW + num;
        String pwResult = "";
        for (int i = 0; i < 4; i++) {
            if (i < curPW.length())
                pwResult += "●";
            else
                pwResult += "○";
        }
        passwordTxt.setText(pwResult);

        if (curPW.length() == 4) {
            checkPassword c = new checkPassword();
            c.execute();
        }
    }

    void deleteString() {
        if (curPW.length() == 0)
            return;
        curPW = curPW.substring(0, curPW.length() - 1);
        String pwResult = "";
        for (int i = 0; i < 4; i++) {
            if (i < curPW.length())
                pwResult += "●";
            else
                pwResult += "○";
        }
        if (curPW.length() == 0)
            passwordTxt.setText("비밀번호를 입력하세요");
        else
            passwordTxt.setText(pwResult);
    }


    private class checkPassword extends AsyncTask<Void, Void, String> {
        String TAG = "GetClassSendPost !!";

        int pwResult;

        checkPassword() {
        }

        protected String doInBackground(Void... unused) {
            String content = executeClient();
            return content;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            if (pwResult == 1) {
                Intent intent = new Intent(PWActivity.this, InitActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            } else {
                passwordTxt.setText("잘못된 비밀번호!");
                curPW = "";
            }
        }

        private String getPWInfo() {
            Log.d(TAG, "getClass post start");

            ArrayList<NameValuePair> post = new ArrayList<NameValuePair>();
            post.add(new BasicNameValuePair("check", "1"));
            post.add(new BasicNameValuePair("appPassword", curPW));

            // Log.e("json data", data.toString());
            // 연결 HttpClient 객체 생성
            HttpClient client;
            client = new DefaultHttpClient();

            // 객체 연결 설정 부분, 연결 최대시간 등등
            HttpParams params = client.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 10000);
            HttpConnectionParams.setSoTimeout(params, 10000);

            // Post객체 생성
            HttpPost httpPost = new HttpPost(Data.SERVER_ADDRESS + "/check_pw_app.php");
            try {

                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(post, "UTF-8");
                httpPost.setEntity(entity);
                HttpResponse response = client.execute(httpPost);
                HttpEntity http_entity = response.getEntity();
                return EntityUtils.toString(http_entity);

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        // 실제 전송하는 부분
        public String executeClient() {
            String respond;
            respond = getPWInfo();

            Log.d(TAG, respond);
            try {
                JSONObject json = new JSONObject(respond);
                pwResult = json.getInt("result");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}
